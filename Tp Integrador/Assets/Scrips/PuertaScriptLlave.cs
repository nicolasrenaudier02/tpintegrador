using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaScriptLlave : MonoBehaviour
{
    public Transform door;
    public float doorSpeed = 1f;
    public int requiredKeys = 1; // N�mero de llaves necesarias para abrir la puerta
    public bool isUnlocked = false; // Indica si la puerta est� desbloqueada
    public Transform openTransform;
    public Transform closedTransform;
    Vector3 targetPosition;
    float time;
    int collectedKeys = 0; // N�mero de llaves recolectadas
    bool isPlayerInsideTrigger = false; // Indica si el jugador est� dentro del trigger

    void Start()
    {
        targetPosition = closedTransform.position;
    }

    void Update()
    {
        if (isUnlocked && isPlayerInsideTrigger && door.position != openTransform.position)
        {
            door.transform.position = Vector3.MoveTowards(door.transform.position, openTransform.position, doorSpeed * Time.deltaTime);
        }
        else if (!isUnlocked && door.position != closedTransform.position)
        {
            door.transform.position = Vector3.MoveTowards(door.transform.position, closedTransform.position, doorSpeed * Time.deltaTime);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            isPlayerInsideTrigger = true;

            if (collectedKeys >= requiredKeys)
            {
                isUnlocked = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            isPlayerInsideTrigger = false;
            isUnlocked = false;
        }
    }

    public void CollectKey()
    {
        collectedKeys++; // Incrementa el contador de llaves recolectadas

        if (collectedKeys >= requiredKeys)
        {
            isUnlocked = true;
        }
    }
}
