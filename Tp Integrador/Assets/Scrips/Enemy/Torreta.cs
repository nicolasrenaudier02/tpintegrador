using UnityEngine;
using UnityEngine.UI;
public class Torreta : MonoBehaviour

{
    public Transform jugador;
    public GameObject balaPrefab;
    public float tiempoDisparo = 2f;
    public float rango = 10f;
    private float tiempoPasado = 0f;
    public float vida = 100f;
    public float velocidadAtaque = 1f;
    public GameObject objetoParaDejarCae;
    public Transform puntoDeCaida;

    public Image fillLifeImage; // Asigna manualmente desde el Inspector
    private bool hasDroppedItem = false;

    // Asigna la referencia fillLifeImage en el Inspector arrastrando y soltando la imagen correspondiente.
    // Aseg�rate de que esta referencia est� enlazada antes de ejecutar el juego.

    void Start()
    {
        // Llena la referencia fillLifeImage desde el Inspector
    }

    void Update()
    {
        tiempoPasado += Time.deltaTime;

        if (Vector3.Distance(transform.position, jugador.position) <= rango && tiempoPasado >= tiempoDisparo)
        {
            tiempoPasado = 0f;
            Disparar();
        }
        transform.LookAt(jugador.position);
        if (vida <= 0)
        {
            Destroy(gameObject);
        }

        tiempoDisparo -= Time.deltaTime * velocidadAtaque;

        // Actualiza la barra de vida en funci�n de la vida restante
        if (fillLifeImage != null)
        {
            float fillAmount = vida / 100f; // Suponiendo que 100 es la vida m�xima
            fillLifeImage.fillAmount = fillAmount;
        }
    }

    void Disparar()
    {
        GameObject bala = Instantiate(balaPrefab, transform.position, transform.rotation);
        Rigidbody balaRigidbody = bala.GetComponent<Rigidbody>();
        balaRigidbody.velocity = (jugador.position - transform.position).normalized * 10f;
        balaRigidbody.useGravity = false; // Establece la gravedad en cero
        Destroy(bala, 2f);

        tiempoDisparo = 1f / velocidadAtaque;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("BalaJugador"))
        {
            vida -= 30;
        }
    }

    private void OnDestroy()
    {
        if (objetoParaDejarCae != null && puntoDeCaida != null && !hasDroppedItem)
        {
            Instantiate(objetoParaDejarCae, puntoDeCaida.position, puntoDeCaida.rotation);
            hasDroppedItem = true;
        }
    }
}