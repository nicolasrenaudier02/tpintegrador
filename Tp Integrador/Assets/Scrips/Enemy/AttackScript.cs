using UnityEngine;

public class AttackScript : MonoBehaviour
{
    public int liveCost = 1;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerHealth playerHealth = other.GetComponent<PlayerHealth>();
            if (playerHealth != null)
            {
                playerHealth.ReceiveDamage(liveCost);
            }

            EnemyScript enemyScript = GetComponentInParent<EnemyScript>();
            if (enemyScript != null)
            {
                enemyScript.KillMe();
            }
        }
    }
}
