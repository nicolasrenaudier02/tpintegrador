using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bot : MonoBehaviour
{
    public int hp = 40;
    public ControlJugador jugador;
    private Rigidbody rb;
    public float rapidezDesplazamiento = 4f;
    public float rangoPersecucion = 10f;
    private bool enPersecucion = false;

    // Agregar referencias para la barra de vida del bot
    public Image fillLifeImage;
    private float maxHP;

    void Start()
    {
        jugador = FindObjectOfType<ControlJugador>();
        rb = GetComponent<Rigidbody>();

        // Inicializar la barra de vida
        maxHP = hp;
    }

    void Update()
    {
        // Calcular la distancia entre el enemigo y el jugador
        float distancia = Vector3.Distance(transform.position, jugador.transform.position);

        // Si la distancia es menor o igual al rango de persecución, activar la persecución
        if (distancia <= rangoPersecucion)
        {
            enPersecucion = true;
        }
        else
        {
            enPersecucion = false;
        }

        if (enPersecucion)
        {
            // Obtener la dirección hacia el jugador
            Vector3 direccion = (jugador.transform.position - transform.position).normalized;

            // Mover el enemigo hacia el jugador a la velocidad deseada
            rb.MovePosition(transform.position + direccion * rapidezDesplazamiento * Time.deltaTime);

            // Mirar hacia el jugador
            transform.LookAt(jugador.transform.position);
        }

        // Actualizar la barra de vida en función de la vida restante
        if (fillLifeImage != null)
        {
            float fillAmount = hp / maxHP;
            fillLifeImage.fillAmount = fillAmount;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("BalaJugador"))
        {
            recibirDaño();
        }
    }

    public void recibirDaño()
    {
        hp = hp - 1;

        if (hp <= 0)
        {
            desaparecer();
        }
    }

    private void desaparecer()
    {
        Destroy(gameObject);
    }
}
