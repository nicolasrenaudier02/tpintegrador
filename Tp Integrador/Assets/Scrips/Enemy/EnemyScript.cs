using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class EnemyScript : MonoBehaviour
{
    NavMeshAgent agent;
    Transform target;
    public float distanceToPursuit = 5f;
    public Transform[] waypoints;
    int currentWaypoint = 0;
    public int hp = 40;

    public GameObject objetoParaDejarCae;
    public Transform puntoDeCaida;

    public Image fillLifeImage; // Referencia al componente Image para la barra de vida

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        if (Vector3.Distance(transform.position, target.position) < distanceToPursuit)
        {
            agent.destination = target.transform.position;
        }
        else
        {
            if (!agent.hasPath)
            {
                if (currentWaypoint < waypoints.Length - 1)
                {
                    currentWaypoint++;
                }
                else
                {
                    currentWaypoint = 0;
                }
                agent.destination = waypoints[currentWaypoint].position;
            }
        }
    }

    public void KillMe()
    {
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("BalaJugador"))
        {
            TakeDamage(1);
        }
        if (other.gameObject.CompareTag("granada"))
        {
            TakeDamage(10);
        }
    }

    public void TakeDamage(int damage)
    {
        hp -= damage;

        if (hp <= 0)
        {
            KillMe();
        }

        // Actualiza la barra de vida en funci�n del da�o recibido
        if (fillLifeImage != null)
        {
            float fillAmount = (float)hp / 40f; // Suponiendo que 40 es la vida m�xima
            fillLifeImage.fillAmount = fillAmount;
        }
    }
}
