using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Escenas : MonoBehaviour
{
    public void Menu()
    {
        SceneManager.LoadScene(0);
    }

    public void Opciones()
    {
        SceneManager.LoadScene(1);
    }

    public void juego1()
    {
        SceneManager.LoadScene(2);
    }
    public void juego2()
    {
        SceneManager.LoadScene(3);
    }


    public void Salir()
    {
        Debug.Log("Salir.");
    }
}

