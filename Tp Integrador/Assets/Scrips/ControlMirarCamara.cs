using UnityEngine;

public class ControlMirarCamara : MonoBehaviour
{
    public float sensibilidad = 5.0f;
    public float suavizado = 2.0f;

    private GameObject jugador;
    private Vector2 mouseMirar;
    private Vector2 suavidadV;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        jugador = transform.parent.gameObject;
    }

    void Update()
    {
        float mdx = Input.GetAxisRaw("Mouse X");
        float mdy = Input.GetAxisRaw("Mouse Y");

        mdx = mdx * sensibilidad * suavizado;
        mdy = mdy * sensibilidad * suavizado;

        suavidadV.x = Mathf.Lerp(suavidadV.x, mdx, 1f / suavizado);
        suavidadV.y = Mathf.Lerp(suavidadV.y, mdy, 1f / suavizado);

        mouseMirar += suavidadV;
        mouseMirar.y = Mathf.Clamp(mouseMirar.y, -90f, 90f);

        transform.localRotation = Quaternion.AngleAxis(-mouseMirar.y, Vector3.right);
        jugador.transform.localRotation = Quaternion.AngleAxis(mouseMirar.x, jugador.transform.up);
    }
}
