using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerRevive : MonoBehaviour
{
    private Transform lastTeleporter; // Referencia al �ltimo teletransportador

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Teletransportador")) // Verificar si colision� con un teletransportador
        {
            lastTeleporter = other.transform;
        }
    }

    public void RevivePlayer()
    {
        if (lastTeleporter != null)
        {
            transform.position = lastTeleporter.position; // Posicionar al jugador en la posici�n del �ltimo teletransportador
        }
        else
        {
            // Si no se ha agarrado ning�n teletransportador, colocar al jugador en una posici�n predeterminada
            transform.position = Vector3.zero;
        }

        // Restaurar la salud del jugador y realizar otras acciones de reinicio necesarias
        // ...

        // Reiniciar la escena para asegurar que todo est� en un estado inicial
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
