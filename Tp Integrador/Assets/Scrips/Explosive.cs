using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosive : MonoBehaviour
{
    public float explosionRadius = 5f;
    public int damage = 30;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("BalaJugador"))
        {
            // Destruir el explosivo
            Destroy(gameObject);
            // Ejecutar la explosi�n
            Explode();
        }
    }

    private void Explode()
    {
        // Obtener todos los colliders en el rango de explosi�n
        Collider[] colliders = Physics.OverlapSphere(transform.position, explosionRadius);

        foreach (Collider collider in colliders)
        {
            if (collider.CompareTag("Enemy"))
            {
                // Aplicar da�o al enemigo
                collider.GetComponent<EnemyScript>()?.TakeDamage(damage);
            }
        }
    }
}
