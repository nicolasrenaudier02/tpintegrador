using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public int initialLives = 5;
    private int lives;
    public Text livesText;
    private Vector3 lastTeleportPosition;
    private string currentScene;

    void Start()
    {
        lives = initialLives;
        livesText.text = lives.ToString();
        currentScene = SceneManager.GetActiveScene().name;
    }

    public void SetLastTeleportPosition(Vector3 position)
    {
        lastTeleportPosition = position;
    }

    public void ReceiveDamage(int liveCost)
    {
        lives -= liveCost;
        livesText.text = lives.ToString();
        if (lives <= 0)
        {
            Death();
        }
    }

    void Death()
    {
        lives = initialLives; // Reiniciar las vidas a su valor inicial

        // Verificar si el jugador est� en la lava
        if (IsPlayerInLava())
        {
            lives = 0; // Establecer la vida en 0 si est� en la lava
        }

        // Cargar la escena actual nuevamente
        SceneManager.LoadScene(currentScene);
    }

    bool IsPlayerInLava()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 1f);
        foreach (Collider collider in colliders)
        {
            if (collider.CompareTag("Lava"))
            {
                return true;
            }
        }
        return false;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Vida"))
        {
            if (lives < 5)
            {
                lives++;
                livesText.text = lives.ToString();
            }
            Destroy(other.gameObject);
        }
    }





}





