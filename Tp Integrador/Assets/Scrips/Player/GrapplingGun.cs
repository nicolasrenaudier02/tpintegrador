using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplingGun : MonoBehaviour
{
    [Header("Datos")]
    public LayerMask queSeEngancha;
    public Transform puntaArma, camara, jugador;
    public KeyCode Swing = KeyCode.Mouse1;
    public LineRenderer lr;

    private Rigidbody rb;
    private Vector3 grapplePoint;
    private float distanciaMaxima = 100f;
    private SpringJoint joint;
    public ControlJugador cj;
    public float fuerzaHaciaArriba = 5f;

    private void Awake()
    {
        lr = GetComponent<LineRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(Swing))
        {
            empezarGrapple();
        }

        if (Input.GetKeyUp(Swing))
        {
            detenerGrapple();
        }
    }

    void LateUpdate()
    {
        dibujarDisparo();

        if (cj.grappling && joint != null)
        {
            rb.AddForce(Vector3.up * fuerzaHaciaArriba);
        }
    }

    void empezarGrapple()
    {
        RaycastHit hit;
        if (Physics.Raycast(camara.position, camara.forward, out hit, distanciaMaxima, queSeEngancha))
        {
            grapplePoint = hit.point;
            joint = jugador.gameObject.AddComponent<SpringJoint>();
            joint.autoConfigureConnectedAnchor = false;
            joint.connectedAnchor = grapplePoint;

            float distanciaDelPunto = Vector3.Distance(jugador.position, grapplePoint);

            joint.maxDistance = distanciaDelPunto * 0.8f;
            joint.minDistance = distanciaDelPunto * 0.25f;

            joint.spring = 4.5f;
            joint.damper = 7f;
            joint.massScale = 4.5f;

            cj.grappling = true;
            lr.positionCount = 2;
        }
    }

    void dibujarDisparo()
    {
        if (!joint)
        {
            return;
        }
        lr.SetPosition(0, puntaArma.position);
        lr.SetPosition(1, grapplePoint);

    }

    void detenerGrapple()
    {
        cj.grappling = false;

        lr.positionCount = 0;
        Destroy(joint);
    }
}
