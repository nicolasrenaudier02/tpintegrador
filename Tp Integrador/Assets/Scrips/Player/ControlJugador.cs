using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class ControlJugador : MonoBehaviour
{
    public float speed = 10.0f;
    public float vida = 100f;
    public GameObject proyectil;
    public Camera camaraPrimeraPersona;
    private Rigidbody rb;
    private Vector3 movimiento;
    public float saltovel;
    private bool enElSuelo = true;
    public int maxSalto = 2;
    public int saltoActual = 0;
    public float magnitudSalto;
    public bool grappling = false;
    public float fuerzaDisparo = 15f; // Nueva variable: potencia del disparo
    public float wallrunSpeed;

    public MovementState state;
    public enum MovementState
    {
        wallrunning,
    }
    public bool wallrunning;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        vida = 100;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        if (vida <= 0)
        {
            SceneManager.LoadScene(0);
        }

        movimiento = new Vector3(moveHorizontal, 0.0f, moveVertical).normalized;

        if (Input.GetButtonDown("Jump") && (enElSuelo || maxSalto > saltoActual))
        {
            rb.velocity = new Vector3(0f, saltovel, 0f * Time.deltaTime);
            rb.AddForce(Vector3.up * saltovel, ForceMode.Impulse);
            enElSuelo = false;
            saltoActual++;
        }
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

            GameObject pro;
            pro = Instantiate(proyectil, ray.origin, transform.rotation);

            Rigidbody rb = pro.GetComponent<Rigidbody>();
            rb.AddForce(camaraPrimeraPersona.transform.forward * fuerzaDisparo, ForceMode.Impulse);

            Destroy(pro, 5);
        }
    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + transform.TransformDirection(movimiento) * speed * Time.fixedDeltaTime);
    }

    private void StateHandler()
    {
        if (wallrunning)
        {
            state = MovementState.wallrunning;
            speed = wallrunSpeed;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bala"))
        {
            vida -= 5;
        }
        if (other.gameObject.CompareTag("granada"))
        {
            vida -= 20;
        }
        if (other.gameObject.CompareTag("BalaTorreta"))
        {
            PlayerHealth playerHealth = GetComponent<PlayerHealth>();
            if (playerHealth != null)
            {
                playerHealth.ReceiveDamage(1);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        enElSuelo = true;
        saltoActual = 0;

        if (collision.gameObject.CompareTag("granada"))
        {
            vida -= 20;
        }
    }
}
