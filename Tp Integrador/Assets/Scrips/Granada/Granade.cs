using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Granade : MonoBehaviour
{

    public float delay = 3;
    float countdown;
    public float radius = 5;
    public float explosionForce = 0;
    bool exploded = false;
    public GameObject explosionEffect;
    public GameObject explosion;
    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;

    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;
        if (countdown <= 0 && exploded == false)
        {
            Exploded();
            exploded = true;
        }
    }

    void Exploded()
    {
        Instantiate(explosionEffect, transform.position, transform.rotation);
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius);
        foreach (var rangeObjects in colliders)
        {
            Rigidbody rb = rangeObjects.GetComponent<Rigidbody>();
            if (rb != null)
            {
                rb.AddExplosionForce(explosionForce * 10, transform.position, radius);
            }
        }
        Instantiate(explosion,transform.position, transform.rotation);
        //Destroy(explosion);
        Destroy(gameObject);
    }
}