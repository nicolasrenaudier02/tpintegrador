using UnityEngine;

public class DronScript : MonoBehaviour
{
    public Camera playerCamera;
    public Camera dronCamera;
    public float movementSpeed = 5f;
    public float rotationSpeed = 100f;
    public float ascendSpeed = 5f;
    public float descendSpeed = 5f;
    public ControlJugador playerScript;

    private bool isActivated = false;
    private bool isDronCameraActive = false;
    private Vector3 initialPlayerPosition; // Posici�n inicial del jugador

    private void Start()
    {
        // Desactivamos la c�mara del dron al iniciar el juego
        dronCamera.gameObject.SetActive(false);
        // Encontramos la instancia del script del jugador
        playerScript = GameObject.FindWithTag("Player").GetComponent<ControlJugador>();
        initialPlayerPosition = playerScript.transform.position; // Guardamos la posici�n inicial del jugador
    }

    private void Update()
    {
        // Si se presiona la tecla C y el dron no est� activado, activamos el dron
        if (Input.GetKeyDown(KeyCode.C) && !isActivated)
        {
            isActivated = true;
            isDronCameraActive = true;
            playerCamera.gameObject.SetActive(false);
            dronCamera.gameObject.SetActive(true);
            // Deshabilitamos el script del jugador
            playerScript.enabled = false;

            // Posicionamos el dron en la posici�n del jugador
            transform.position = playerScript.transform.position;
        }
        // Si se presiona la tecla C y el dron est� activado, desactivamos el dron y volvemos a la c�mara del jugador
        else if (Input.GetKeyDown(KeyCode.C) && isActivated)
        {
            isActivated = false;
            isDronCameraActive = false;
            playerCamera.gameObject.SetActive(true);
            dronCamera.gameObject.SetActive(false);
            // Habilitamos el script del jugador
            playerScript.enabled = true;

            // Devolvemos el dron a la posici�n del jugador
            transform.position = initialPlayerPosition;
        }

        // Si el dron est� activado, procesamos sus controles
        if (isActivated)
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            // Movimiento del dron en los ejes X y Z
            transform.Translate(Vector3.right * horizontal * movementSpeed * Time.deltaTime);
            transform.Translate(Vector3.forward * vertical * movementSpeed * Time.deltaTime);

            // Ascenso y descenso del dron en el eje Y
            float ascendDescend = 0f;
            if (Input.GetKey(KeyCode.Space))
            {
                ascendDescend = ascendSpeed;
            }
            else if (Input.GetKey(KeyCode.LeftControl))
            {
                ascendDescend = -descendSpeed;
            }
            transform.Translate(Vector3.up * ascendDescend * Time.deltaTime);
        }
    }
}
