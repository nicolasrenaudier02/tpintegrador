using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public PuertaScriptLlave doorToOpen;

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            doorToOpen.CollectKey();
            Destroy(gameObject);
        }
    }
}
